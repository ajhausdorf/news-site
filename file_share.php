<!DOCTYPE html>
<head>
<title>File Share</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
 <form action="file_share.php" method="GET">
	<p>
		<label for="username">Username:</label>
		<input type="text" name="username" id="username"/>
		<input type="submit" value="Enter"/> 
	</p>
</form>

<?php
	session_start();

	$user = (int) @$_SESSION['inc_num'];

	session_destroy();
?>

<form enctype="multipart/form-data" action="uploader.php" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" />
	</p>
</form>


 
<?php

if ($handle = opendir('upload/')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            echo "$entry<br>";
        }
    }
    closedir($handle);
}   

?>


</div></body>
</html>