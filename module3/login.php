<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.


 
require 'database.php';
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), user_id, password_encrypted FROM users WHERE username=?");
 
 		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
			}

// Bind the parameter
$user = $_POST['username'];
$stmt->bind_param('s', $user);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();

	echo $cnt;
	echo '<br>';
	echo $user_id;
	echo '<br>';
	echo $pwd_hash;
 
$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	// Login succeeded!
	session_start();
	$_SESSION['username'] = $user;
	$_SESSION['user_id'] = $user_id;
	  header('Location: home.php');
	// Redirect to your target page
}else{
	// Login failed; redirect back to the login screen
	// header('Location:main.php');
	echo "Login failed ";
}
?>