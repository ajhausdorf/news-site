<?php

require 'database.php';

$username = $_POST['username'];
$password = crypt($_POST['password'], $_POST['password']);
echo $password;

$stmt = $mysqli->prepare("insert into users (username, password_encrypted) values (?, ?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	echo "Sorry, that username was too long or already taken";
	exit;
}
 

$stmt->bind_param('ss', $username, $password);
 
$stmt->execute();
 
$stmt->close();

$stmt = $mysqli->prepare("SELECT user_id FROM users WHERE username=?");

$stmt->bind_param('ss', $user_id, $username);

$stmt->execute();
 
$stmt->close();

session_start();
$_SESSION['username'] = $username;
$_SESSION['user_id'] = $user_id;

$_SESSION['token'] = substr(md5(rand()), 0, 10);
	header('Location: success2.html');
 
?>