<!DOCTYPE html>
<head>
<title>My Web Page</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
	/* Put code for logic of generating the file path.. */ 
<?php
	session_start();
 
	// Get the filename and make sure it is valid
	$filename = basename($_FILES['uploadedfile']['name']);
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		echo "Invalid filename";
		exit;
	}
	 
	// Get the username and make sure it is valid
	$username = $_SESSION['username'];
	if( !preg_match('/^[\w_\-]+$/', $username) ){
		echo "Invalid username";
		exit;
	}
	 
	$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
	 
	if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
		header("Location: file_share.php");
		exit;
	}else{
		header("Location: upload_failure.html");
		exit;
	}
?>

</div></body>
</html>